# Ansible for Networking

This is my testing area for my in-house lab about Ansible

## Installation


```python

# install ansible and python netaddr library using pip
pip install ansible==3.0
sudo yum install python-netaddr

# additional tools
Install Ansible 3.0
Install Paramiko 3.0
Install Python Netaddr

# ansible galaxy collections for your automation environment 
ansible-galaxy collection install cisco.ios
ansible-galaxy collection install cisco.iosxr
ansible-galaxy collection install cisco.nxos

```

# Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.


