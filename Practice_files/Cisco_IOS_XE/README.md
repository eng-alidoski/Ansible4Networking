# Cisco IOS-XE Automation using RESTCONF and NETCONF

This guide explains how to automate the configuration and verification of Cisco IOS-XE devices using RESTCONF and NETCONF protocols. These protocols are used for network automation and management and provide a programmatic way to interact with network devices.

![](https://www.google.com/url?sa=i&url=https%3A%2F%2Fslideplayer.com%2Fslide%2F12153228%2F&psig=AOvVaw1i_k2tUJpubI6oFm0p_hV2&ust=1682958210859000&source=images&cd=vfe&ved=0CBEQjRxqFwoTCIiR8YSC0v4CFQAAAAAdAAAAABAO)

# Prerequisites

- A Cisco IOS-XE device with RESTCONF and/or NETCONF support
- A network automation tool such as Ansible, Python, or Postman
- Knowledge of REST APIs and/or NETCONF protocols

# Enabling RESTCONF and NETCONF on Cisco IOS-XE

To enable RESTCONF and NETCONF on a Cisco IOS-XE device, follow the below steps:
1- Configure the device's hostname, domain name, and enable a domain name system (DNS) server:

```
hostname <hostname>
ip domain-name <domain-name>
ip name-server <DNS-server-IP>
```

2- Configure a username and password for RESTCONF and NETCONF:
```
username <username> privilege 15 secret <password>
```

3- Configure the device to use a self-signed certificate for secure communication:
```
crypto key generate rsa general-keys label <label> modulus <modulus>
```

4- Create a trustpoint and configure the self-signed certificate:
```
crypto pki trustpoint <trustpoint-name>
enrollment selfsigned
subject-name cn=<hostname>
rsakeypair <label>
```

5- Enable the device to listen for RESTCONF and NETCONF requests:
```
netconf-yang
restconf
```





# Contributing
If you have any Cisco IOS configurations you'd like to contribute, please submit a pull request! i welcome contributions from anyone who wants to share their knowledge and help others in the networking community.
