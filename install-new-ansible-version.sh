#This script will uninstall old version of ansible 2.9.4 on sandbox and install new version 3.0

deactivate
sudo pip uninstall ansible==2.9.4 -y
source py3venv/bin/activate
pip install ansible==3.0
sudo yum install python-netaddr
